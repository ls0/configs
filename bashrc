#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '

export PATH="$PATH:/home/lewis/.local/bin"

export QT_QPA_PLATFORMTHEME=qt5ct

exec fish 
